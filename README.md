It's meant to be a humble C++ matrix(2d) library. Although, at least the following goals are desired to be achieved:

-overload constructors, destructors, typecast and assign operators.

-implement the +, -, /, *, [] and << operators.

-use the templates to make generic functions about the data type (int, double, float, etc.).

-implement a print function, try to create a standard identity matrix.

-use the standard rules in equations with different data-types.

-change the sets and gets to the cpp file.

-overload == and != operator.

-make a c++11 version using the "auto" keyword.
